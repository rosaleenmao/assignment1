

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class TrafficCameraReportServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	static HashMap<String, String> previousRequests;
	
	public TrafficCameraReportServlet() {

		previousRequests = new HashMap<String, String>();	
		
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
	                throws ServletException, IOException {
			
		if (request == null) {
			return;
		}
		
		String givenETag = request.getHeader("If-Match");
		
		if (givenETag != null && previousRequests.containsKey(givenETag)) {
			String correctResponse = previousRequests.get(givenETag);
			if (correctResponse.charAt(0) == 'N') {
				response.setHeader("Content-Type", "text/plain");
			    response.setHeader("ETag", givenETag);
			}
			response.getWriter().println(correctResponse);
			return;
		}

		String query = request.getQueryString();
		if (query != null && previousRequests.containsKey(query)) {
			String correctResponse = previousRequests.get(query);
			if (correctResponse.charAt(0) == 'N') {
				response.setHeader("Content-Type", "text/plain");
			    response.setHeader("ETag", query);
			}
			response.getWriter().println(correctResponse);
			return;
		}
		
		if (query == null) {
			previousRequests.put(query, "Enter a single paramter.");
			response.getWriter().println("Enter a single paramter.");
			return;
		}
		
		if (query.contains("&")) {
			previousRequests.put(query, "Error. Pass only one parameter");
			response.getWriter().println("Error. Pass only one parameter");
			return;
		}
		
		int equalsPosition = -1;
		
		for (int i = 0; i < query.length() && equalsPosition == -1; i++) {
			if (query.charAt(i) == '=') {
				equalsPosition = i;
			}
		}
		
		String param = null;
		
		if (equalsPosition != -1) {
			param = query.substring(0, equalsPosition);
		}
		
		if (param == null) {
			previousRequests.put(query, "Enter a single paramter.");
			response.getWriter().println("Enter a single parameter.");
			return;
		}

		if (!checkParameter(param)) {
			previousRequests.put(query, "Provided query parameter is not supported.");
			response.getWriter().println("Provided query parameter is not supported.");
			return;
		}
		
		String originalValue = query.substring(equalsPosition + 1);
		
		String value = request.getParameter(param);
		
		if (value == "") {
			previousRequests.put(query, "Enter a value.");
			response.getWriter().println("Enter a value.");
			return;
		}
	    
		String correctResponse = "Number of cameras with " + param + " '" + originalValue + "': " + DOMParser(param, value);
		previousRequests.put(query, correctResponse);
	    response.getWriter().println(correctResponse);
	    
	    response.setHeader("Content-Type", "text/plain");
	    response.setHeader("ETag", query);
	}
	
	boolean checkParameter(String param) {
		return  param.equals("camera_status") ||
				param.equals("camera_mfg") ||
				param.equals("location_type") ||
				param.equals("ip_comm_status");
	}
	
	//adapted from the parser method from professor's github
	int DOMParser(String param, String value) {
		int correctNodes = 0;
		
		DocumentBuilderFactory builderFactory =
		        DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
		    builder = builderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
		    e.printStackTrace();  
		}
		
		Document document = null;
		try {
			document = builder.parse(new URL("http://www.cs.utexas.edu/~devdatta/traffic_cameras_data.xml").openStream());
		} catch (SAXException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
		Element rootElement = document.getDocumentElement();
		Queue<Element> q = new LinkedList<Element>();
		q.add(rootElement);
		
		while(!q.isEmpty()) {
			Element e = (Element) q.remove();
			String nodeName = e.getNodeName().toLowerCase();
			nodeName = nodeName.substring(3);
			if (nodeName.equals(param)) {
		    	String nodeValue = e.getTextContent();
		    	if (nodeValue.toLowerCase().equals(value.toLowerCase())) {
		    		correctNodes++;
		    	}
		    }
			NodeList nodes = e.getChildNodes();
			for(int i=0; i<nodes.getLength(); i++) {
				  Node node = nodes.item(i);
				  if(node instanceof Element) {
					  q.add((Element) node);
				    }
				  }
			}
		return correctNodes;
	}
}
